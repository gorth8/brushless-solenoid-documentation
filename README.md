# Brushless Solenoid Documentation
This repo is documentation for my brushless flywheel and solenoid pusher conversion stryfe. 

The stryfe has both a pusher trigger and a rev trigger. The pusher can be triggered without using the rev trigger as the flywheels will spin up before the shot is fired. The rev trigger is useful for quicker shots when engagement is expected.

The stryfe has a select fire switch with 3 positions. The positions are semi-auto, burst, and full-auto. I use 2 shot burst because I don't think 3 is more useful and it is dart hungry.

I run the stryfe on 4s because I think the solenoid is much more reliable at that voltage.

## Repo Contents Description
- `brushless_solenoid.ino`: Code for Arduino
- `circuit.cddx`: Circuit Diagram File
- `circuit.png`: Circuit Diagram Image

## 3D Model Resources
- [Ultrasonic2 Brushless Stryfe Cage gen4](https://www.thingiverse.com/thing:3433552)
- [Ultrasonic2 Brushless Flywheels](https://www.thingiverse.com/thing:2585080)
- [Airzone Solenoid Cage for Stryfe](https://www.thingiverse.com/thing:3694734)

## Misc Reccomendations
- Use the wheels with the lowest crush you can to acheive target speed. This is because higher crush tends to jam more and problems can occur if a hard tip dart is accidentally loaded.
- Use the smallest battery you can considering current requirements and playtime. I use a huge battery because I want it to last for an all day game. This is unneccessary for 2 hour hvz.
- A smaller solenoid is now available.

## Future Improvements
- Incorporate dart sensor. The magazines I use are by far the least reliable part of this blaster. I make the time between shots generous to account for this. More reliable firing could be acheived with a sensor that only allows firing once a dart is in place.
- Smaller battery holder. Currently I store a large battery in the stock. The option to use a smaller battery and without a stock would be useful for shorter games, but something must be designed to do this.
