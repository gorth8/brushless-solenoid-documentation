#include <Servo.h>

// pin definitions
const int trigger = 3;
const int pusher = 5;
const int rev = 7;
const int motor = 9;
const int switch1 = 11;
const int switch2 = 12;
const int potPin = A0;
Servo myservo;

// SW limits
const int maxServo = 1800;
const int revDownTime = 1000;
const int pusherForwardDelay = 50;
const int pusherBackDelay = 70;
const int minRevUp = 200;

//misc
float timeLastRev = 0;

void setup() {
  // set pins
  pinMode(trigger, INPUT);
  pinMode(rev, INPUT);
  pinMode(switch1, INPUT);
  pinMode(switch2, INPUT);
  pinMode(pusher, OUTPUT);

  // setup motors
  myservo.attach(motor);
  myservo.write(1000);
  delay(2000);

  // serial setup
  Serial.begin(57600);

}

int readPot() {
  //read pot
  float pot = analogRead(potPin); // 0-1023

  //convert to signal
  int result = map(pot, 0, 1023, 1000, maxServo);
  return result;
}

int revDown(int currentSpeed) {
  float timeSince = millis() - timeLastRev;
  Serial.print("timeSince = ");
  Serial.print(timeSince);
  if (timeSince > revDownTime or timeSince <= 0) {
    return 1000; // off
  }
  else {
    //calculate speed for rev down
    int newSpeed = map(timeSince, 0, revDownTime, currentSpeed, 1000);
    Serial.print("\tnewSpeed = ");
    Serial.println(newSpeed);
    return newSpeed;
  }
}

void loop() {
  //update from pot
  int speedSetting = readPot();
  Serial.println(speedSetting);

  //determine mode (0,0)=burst
  Serial.print("switch1: ");
  Serial.print(digitalRead(switch1));
  Serial.print("\tswitch2: ");
  Serial.println(digitalRead(switch2));

  if (digitalRead(trigger) == HIGH) {
    // trigger pulled
    if (digitalRead(rev) == LOW) {
      //need to rev first
      myservo.write(speedSetting);
      delay(minRevUp);
    }
    //determine mode
    if (digitalRead(switch1) == LOW && digitalRead(switch2) == LOW) {
      //burst fire
      digitalWrite(pusher, HIGH);
      delay(pusherForwardDelay);
      digitalWrite(pusher, LOW);
      delay(pusherBackDelay);
      digitalWrite(pusher, HIGH);
      delay(pusherForwardDelay);
      while (digitalRead(trigger) == HIGH) {
        delay(10);
      }
      digitalWrite(pusher, LOW);
      delay(pusherBackDelay);
    }
    else {
      if (digitalRead(switch1) == HIGH) {
        //semi auto
        digitalWrite(pusher, HIGH);
        delay(pusherForwardDelay);
        while (digitalRead(trigger) == HIGH) {
          delay(10);
        }
        digitalWrite(pusher, LOW);
        delay(pusherBackDelay);

      }
      else {
        //full auto
        while (digitalRead(trigger) == HIGH) {
          digitalWrite(pusher, HIGH);
          delay(pusherForwardDelay);
          digitalWrite(pusher, LOW);
          delay(pusherBackDelay);
        }
      }
    }
  }

  Serial.print("rev = ");
  Serial.println(digitalRead(rev));
  // rev trigger
  if (digitalRead(rev) == HIGH) {
    //rev up
    myservo.write(speedSetting);
    timeLastRev = millis();
  }
  else {
    //rev down
    myservo.write(revDown(speedSetting));
  }


  delay(10);

}
